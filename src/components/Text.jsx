import React from "react";

class Text extends React.Component {
  render() {
    return <span style={{ color: this.props.color }}>{this.props.value}</span>;
  }
}

export default Text;
