import React from "react";
import Text from "./components/Text";

class App extends React.Component {
  render() {
    return (
      <h1>
        Hello, <Text color="Blue" value="Illian Curvello" />
      </h1>
    );
  }
}

export default App;
